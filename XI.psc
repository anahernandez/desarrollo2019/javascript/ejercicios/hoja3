Algoritmo tres_num
	a <- 0
	b <- 0
	c <- 0
	mayor <- 0
	mediano <- 0
	menor <- 0
	Leer a
	Leer b
	Leer c
	Si a>b Entonces
		Si b>c Entonces
			mayor = a
			mediano = b
			menor = c
		SiNo
			mayor = a
			Si a>c Entonces
				mediano = c
				menor = b
			SiNo
				mediano = b
				menor = c
			FinSi
		FinSi
	SiNo
		Si a>c Entonces
			mayor = b
			mediano = a
			menor = c
		SiNo
			menor = a
			Si b>c Entonces
				mayor = b
				mediano = c
			SiNo
				mayor = c
				mediano = b
			FinSi
		FinSi
	FinSi
	Escribir "Los n�meros introducidos en orden ascendente son:" menor, mediano, mayor
	Escribir "Los n�meros introducidos en orden descendente:" mayor, mediano, menor
FinAlgoritmo

